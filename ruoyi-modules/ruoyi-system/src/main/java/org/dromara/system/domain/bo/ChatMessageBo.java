package org.dromara.system.domain.bo;

import org.dromara.system.domain.ChatMessage;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 用户对话记录业务对象 chat_message
 *
 * @author Lion Li
 * @date 2023-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = ChatMessage.class, reverseConvertGenerate = false)
public class ChatMessageBo extends BaseEntity {

    /**
     * id
     */
    private Long id;

    /**
     * 消息内容
     */
    @NotBlank(message = "消息内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 消息发送方
     */
    @NotBlank(message = "消息发送方不能为空", groups = { AddGroup.class, EditGroup.class })
    private String role;

    /**
     * 消息接收方
     */
    @NotBlank(message = "消息接收方不能为空", groups = { AddGroup.class, EditGroup.class })
    private String userId;

    /**
     * 备注
     */
    private String remark;


}
