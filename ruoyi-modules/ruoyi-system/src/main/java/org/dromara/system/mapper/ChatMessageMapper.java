package org.dromara.system.mapper;

import org.dromara.system.domain.ChatMessage;
import org.dromara.system.domain.vo.ChatMessageVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 用户对话记录Mapper接口
 *
 * @author Lion Li
 * @date 2023-05-19
 */
public interface ChatMessageMapper extends BaseMapperPlus<ChatMessage, ChatMessageVo> {

}
