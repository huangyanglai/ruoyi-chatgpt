package org.dromara.system.domain.vo;

import org.dromara.system.domain.ChatMessage;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 用户对话记录视图对象 chat_message
 *
 * @author Lion Li
 * @date 2023-05-19
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = ChatMessage.class)
public class ChatMessageVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 消息内容
     */
    @ExcelProperty(value = "消息内容")
    private String content;

    /**
     * 消息发送方
     */
    @ExcelProperty(value = "消息发送方")
    private String role;

    /**
     * 消息接收方
     */
    @ExcelProperty(value = "消息接收方")
    private String userId;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
