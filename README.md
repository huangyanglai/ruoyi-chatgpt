## 平台简介
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitee.com/dromara/RuoYi-Vue-Plus/blob/master/LICENSE)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com/?from=RuoYi-Vue-Plus)
<br>
[![RuoYi-Vue-Plus](https://img.shields.io/badge/RuoYi_Vue_Plus-5.0.0-success.svg)](https://gitee.com/dromara/RuoYi-Vue-Plus)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-3.0-blue.svg)]()
[![JDK-17](https://img.shields.io/badge/JDK-17-green.svg)]()

> 使用若依plus版本集成chatgpt-前端-用户端

> 本项目完全开源免费！
后台管理界面使用elementUI服务端使用Java17+SpringBoot3.X

实现功能
1. 微信用户免登录
2. 消息历史记录
3. GPT-3.5对话模型
4. 敏感词过滤
>测试功能: AI绘画

>项目地址
<ul>
<li>后端: https://gitee.com/ageerle/ruoyi-chatgpt</li>
<li>小程序端: https://gitee.com/ageerle/ruoyi-uniapp-chatgpt</li>
<li>前端-后台管理: https://gitee.com/ageerle/ruoyi-ui-chatgpt</li>
<li>前端-用户端: https://gitee.com/ageerle/ruoyi-web-chatgpt</li>
<li>演示地址: https://ai.pandarobot.chat/</li>
</ul>

>前端构建运行
<ul>
<li>安装依赖项: pnpm install</li>
<li>本地运行: pnpm dev</li>
<li>打包: pnpm build</li>
</ul>

>常见问题

解决 npm或pnpm : 无法加载文件 C:\Users\hp\AppData\Roaming\npm\cnpm.ps1，因为在此系统上禁止运行脚本
1. 找到Windows PowerShell，点击右键找到更多，找到以管理员身份运行
2. set-ExecutionPolicy RemoteSigned 然后回车 选择：输入A选择全是，或者输入Y选择是 都可以的接着重新启动
   然后去运行就可以了

## 小程序演示
<div>
  <img style="margin:10px" src="./image/03.png" alt="drawing" width="300px" height="400px"/>
  <img style="margin:10px" src="./image/04.png" alt="drawing" width="300px" height="400px"/>
  <img style="margin:10px" src="./image/05.png" alt="drawing" width="300px" height="400px"/>
  <img style="margin:10px" src="./image/06.png" alt="drawing" width="300px" height="400px"/>
</div>

## PC端演示

<div>
  <img style="margin-top:10px" src="./image/07.png" alt="drawing" width="550px" height="300px"/>
  <img style="margin-top:10px" src="./image/08.png" alt="drawing" width="550px" height="300px"/>
</div>

## 进群学习
<div>
  <img src="./image/01.png" alt="drawing" width="300px" height="300px"/>
  <img src="./image/02.png" alt="drawing" width="300px" height="300px"/>
</div>


## 参考项目
<ol>
<li>https://github.com/Grt1228/chatgpt-java</li>
<li>https://gitee.com/shoujing1001/chatgpt-uniapp</li>
<li>https://gitee.com/dromara/RuoYi-Vue-Plus</li>
<li>https://github.com/mjjh1717/chatgpt-shuowen</li>
</ol>
