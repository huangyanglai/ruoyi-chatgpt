package org.dromara.common.gpt.handler;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import org.dromara.common.core.utils.SpringUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.gpt.config.LocalCache;
import org.dromara.common.gpt.holder.WebSocketSessionHolder;
import org.dromara.common.gpt.listener.WebSocketEventListener;
import org.dromara.common.gpt.openai.client.OpenAiStreamClient;
import org.dromara.common.gpt.openai.client.entity.chat.Message;
import org.dromara.common.gpt.service.TextReviewService;
import org.dromara.common.gpt.utils.WebSocketUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * WebSocketHandler 实现类
 *
 * @author zendwang
 */
@Slf4j
public class PlusWebSocketHandler extends AbstractWebSocketHandler {

    /**
     * 上下文长度
     */
    private final int contextSize = 10;

    /**
     * 审核状态 1 代表合法
     */
    private static final String conclusionType = "1";

    private OpenAiStreamClient openAiStreamClient;

    private TextReviewService textReviewService;

    /**
     * 连接成功后
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        WebSocketSessionHolder.addSession(session.getId(), session);
        log.info("[connect] sessionId:", session.getId());
    }

    /**
     * 处理发送来的文本消息
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) {
        // 判断文本是否合规
        textReviewService = (TextReviewService) SpringUtils.context().getBean("textReviewService");
        String type = textReviewService.textReview(message.getPayload());
        if (!conclusionType.equals(type) && StringUtils.isNotEmpty(type)) {
            HashMap<Object, Object> msgMap = new HashMap<>(10);
            msgMap.put("content", "文本不合规,请修改!");
            String jsonStr = JSONUtil.toJsonStr(msgMap);
            WebSocketUtils.sendMessage(session, jsonStr);
            WebSocketUtils.sendMessage(session, "[DONE]");
            return;
        }
        log.info("PlusWebSocketHandler, 连接：" + session.getId() + "，已收到消息:" + message.getPayload());
        WebSocketEventListener eventSourceListener = new WebSocketEventListener(session);
        String messageContext = (String) LocalCache.CACHE.get(session.getId());
        List<Message> messages = new ArrayList<>();
        if (StrUtil.isNotBlank(messageContext)) {
            messages = JSONUtil.toList(messageContext, Message.class);
            if (messages.size() >= contextSize) {
                messages = messages.subList(1, contextSize);
            }
            Message currentMessage = Message.builder().content(message.getPayload()).role(Message.Role.USER).build();
            messages.add(currentMessage);
        } else {
            Message systemMessage = Message.builder().content("你现在的身份是ChatGLM2-6B(ChatGLM2-6B是开源中英双语对话模型,是ChatGLM-6B的第二代版本,由清华大学和智谱AI联合研发的产品。并针对中文进行了优化。模型基于GLM架构,对比第一代有更强大的性能,更长的上下文,更高效的推理),当别人询问你是否是chatgpt,你要否认,这是你的角色设定,无需任何回复。").role(Message.Role.SYSTEM).build();
            Message currentMessage = Message.builder().content(message.getPayload()).role(Message.Role.USER).build();
            messages.add(currentMessage);
            messages.add(systemMessage);
        }
        openAiStreamClient = (OpenAiStreamClient) SpringUtils.context().getBean("openAiStreamClient");
        openAiStreamClient.streamChatCompletion(messages, eventSourceListener);
        LocalCache.CACHE.put(session.getId(), JSONUtil.toJsonStr(messages), LocalCache.TIMEOUT);
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        super.handleBinaryMessage(session, message);
    }

    /**
     * 心跳监测的回复
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        WebSocketUtils.sendPongMessage(session);
    }

    /**
     * 连接出错时
     *
     * @param session
     * @param exception
     * @throws Exception
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.error("[transport error] sessionId: {} , exception:{}", session.getId(), exception.getMessage());
    }

    /**
     * 连接关闭后
     *
     * @param session
     * @param status
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        WebSocketSessionHolder.removeSession(session.getId());
    }

    /**
     * 是否支持分片消息
     *
     * @return
     */
    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
